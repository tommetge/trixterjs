// #var $q = require('q');
var com = require('serialport');
var d = require('./difficulties');
var Event = require('./event');
var CrankChange = require('./crank_change');

var DEBUG = false;

function Trixter(notify) {
  var self = this;
  self.notify = notify;
  self.port = null;

  // timers
  self.control = null;
  self.broadcast = null;

  // parsed event data
  self.raw_data = null;
  self.message = '';
  self.messages_rcvd = 0;
  self.current_event = null;
  self.last_rev_time = null;

  // broadcast data
  self.power = 0;
  self.revs = 0;
  self.last_revs = 0;
  self.last_fly_rpms = 0;

  // control attributes
  self.difficulty = 5;

  this.nextEvent = function(event) {
    if (event.rightGearUp && !self.current_event.rightGearUp) {
      if (self.difficulty < d.Difficulties.length - 1)
        self.difficulty++;
      console.log("difficulty increased to " + self.difficulty);
    }
    if (event.rightGearDown && !self.current_event.rightGearDown) {
      if (self.difficulty > 0)
        self.difficulty--;
      console.log("difficulty decreased to " + self.difficulty);
    }
    if (event.crankPosition != self.current_event.crankPosition) {
      self.crankPositionChanged(event);
    }
    self.current_event = event;
  };

  this.crankPositionChanged = function(event) {
    past_position = self.current_event.crankPosition;
    position = event.crankPosition;

    // Ignore until we've completed a full revolution
    if (!(position >= 30 && past_position < 30))
      return;

    var forward = false;
    if (past_position < position)
      forward = (position - past_position < 30);
    else
      forward = (past_position - position) > 30;

    // Ignore back-pedaling
    if (!forward)
      return;

    now = new Date() / 1000;
    elapsed = now - self.last_rev_time;
    rpms = 60 / elapsed;
    self.power = self.calculatePower(event, rpms);

    self.revs++;
    self.last_rev_time = now;
  };

  this.calculatePower = function(event, rpms) {
    // The fly adjustment detects and accounts for acceleration
    // and deceleration - if the flywheel is speeding up, a
    // higher wattage is reported. Slowing will report lower.
    fly_adjustment = 1.0;
    fly_rpms = 10000.0 / event.flywheelRotation
    if (self.last_fly_rpms <= 0)
      fly_adjustment = fly_rpms;
    else
      fly_adjustment = self.last_fly_rpms / fly_rpms;

    fly_adjustment = fly_adjustment * fly_adjustment;
    self.last_fly_rpms = fly_rpms;

    // Look up the power rating as determined by the Trixter
    // config file (MainXD30aN.txt).
    power_rpms = 0;
    for (i=0; i<d.PowerCurveRPMs.length; i++) {
      if (rpms < d.PowerCurveRPMs[i])
        break;

      power_rpms = d.PowerCurveRPMs[i];
    }

    if (power_rpms == 0)
      return 0;

    // Trixter config defines 25 levels of difficulty. We only
    // use 16.
    difficulty_offset = d.PowerMappings[self.difficulty];
    // difficulty_offset = Math.floor((25/15) * self.difficulty);
    raw_watts = d.PowerCurves[power_rpms][difficulty_offset];

    return fly_adjustment * raw_watts;
  };

  this.parseByte = function(byte) {
    if (self.message.length == 32) {
      if (DEBUG) console.log(self.message);
      event = new Event(self.message);
      if (self.current_event) {
        self.nextEvent(event);
      } else {
        self.current_event = event;
      }

      self.message = '';
      self.messages_rcvd++;
      if (self.messages_rcvd % 20 == 0) {
        self.sendControl();
      }
    }

    if (self.message.length == 0) {
      self.message = '';
    }
    if (self.message[0] != '6') {
      self.message = '';
    }
    if (self.message.length > 1 &&
        self.message.substr(0,2) != '6a') {
      self.message = '';
    }
    if (self.message.length > 15 &&
        self.message.substr(12,4) != 'ffff') {
      self.message = '';
	  }

    self.message = self.message.concat(byte);
  }

  this.parseData = function(data) {
    string = data.toString('ascii');
    for (i=0; i<string.length; i++) {
      self.parseByte(string[i]);
    }
  };

  this.sendControl = function() {
    message = d.Difficulties[self.difficulty];
    buff = new Buffer(message, 'hex');
    if (self.port) {
      self.port.write(buff);
    }
  };

  this.sendBroadcast = function() {
    // Don't broadcast unless revs have changed
    if (self.revs == 0 || self.last_revs == self.revs)
      return;

    console.log('watts: ' + self.power, 'rev_count: ' + self.revs);
    self.notify({'watts': self.power, 'rev_count': self.revs});
    self.last_revs = self.revs;
  };
};

Trixter.prototype.open = function(comName) {
  var self = this;
  self.port = new com.SerialPort(comName, {
    baudRate: 115200,
    dataBits: 8,
    stopBits: 1,
    parser: com.parsers.byteLength(32)
  });
  self.port.on('open', function() {
    console.log("Connected to USB");
    self.port.on('close', self.stop);
    self.port.on('data', self.parseData);
    // self.control = setInterval(self.sendControl, 100);
    self.broadcast = setInterval(self.sendBroadcast, 500);
  });
  self.port.on('error', function() {
    console.log("Waiting for USB connection");
    setTimeout(function() { self.open(comName); }, 3000);
  });
};

Trixter.prototype.close = function(callback) {
  if (self.port)
    self.port.close(callback);
}

Trixter.prototype.start = function(callback) {
  var self = this;
  console.log("starting");
  self.open('/dev/ttyUSB0');
  callback();
};

Trixter.prototype.stop = function(callback) {
  var self = this;
  console.log("stopping");
  clearInterval(self.control);
  clearInterval(self.broadcast);
  self.port = null;
}

module.exports = Trixter
