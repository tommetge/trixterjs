function CrankChange(ticks, time, elapsed) {
  var self = this;
  self.ticks = ticks;
  self.time = time;
  self.elapsed = elapsed;
};

module.exports = CrankChange
