function Event(bytes) {
  var self = this;
  self.bytes = bytes;
  self.time = new Date() / 1000;

  self.crankPosition = parseInt(self.bytes.substr(6,2), 16);
  self.flywheelRotation = parseInt(self.bytes.substr(24,4), 16);
  self.leftGearUp = self.bytes[16] == '7';
  self.leftGearDown = self.bytes[18] == '7';
  self.rightGearUp = self.bytes[18] == 'd';
  self.rightGearDown = self.bytes[18] == 'b';
};

module.exports = Event
