var Trixter = require('./trixter.js');
var peripheral = require('ble-cycling-power');
var execFile = require('child_process').execFile

var startBT = function() {
  execFile('/bin/hciconfig', ["hci0", "piscan"], null, function(error, stdout, stderr) {
    if (error) {
      console.log("Error making BT discoverable, waiting 3 seconds and retrying...");
      setTimeout(startBT, 3000);
    } else {
      console.log("BT is discoverable, starting trix...");
      startTrix();
    }
  });
};

var startTrix = function() {
  console.log("Starting trix");
  var ble = new peripheral.BluetoothPeripheral('Trixter XDream', false);
  var trix = new Trixter(ble.notify);
  trix.start(function() {
    console.log("Trix started");
  });
};

var main = function(args) {
  startBT();
};

main(process.argv);
