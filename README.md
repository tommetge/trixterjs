# Trixter

## Setup and Installation

First, disable the bluetooth service:

```sudo systemctl stop bluetooth``` (once)
```sudo systemctl disable bluetooth``` (persist on reboot)

For Raspberry Pi running debian / Ubuntu, the following will install
the needed dependencies:

```sudo apt-get install bluetooth bluez libbluetooth-dev libudev-dev```

We also need to enable 1.2A USB power draw mode, otherwise the RPi limits
the draw to 0.6A:

```
/boot/config.txt
---
# Force 1.2A USB draw
max_usb_current=1
```

Install node:

```
# Update this with the current stable version...
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
sudo apt-get install --yes nodejs
npm install
```

Grant privileges to change bluetooth to node / hciconfig:

```
sudo setcap 'cap_net_raw,cap_net_admin+eip' `which hciconfig`
sudo setcap cap_net_raw+eip $(eval readlink -f `which node`)
```

(Node will make changes to connect BLE servers, hciconfig is needed
to set the bluetooth device in discovery mode.)

Add your user to the dialout group to allow it to access /dev/ttyUSB:

```usermod -a -G dialout [user]```

Install the trixter service:

```
sudo cp trixter.service /etc/systemd/system/trixter.service
sudo systemctl start trixter
sudo systemctl enable trixter
```

If the service starts cleanly, then reboot. You're ready to plug
everything in and start going.

Using a male-to-mail USB cable, plug in the Trixter XDream bike
(the USB plug in the front) to one of the Raspberry Pi's USB
ports. The trixter service will automatically connect to it and
begin to broadcast data via Bluetooth. The easiest way to verify
this is to download and run the Wahoo Fitness app. Tap "Sensors",
"Add New Sensor", and you should see Trixter XDream. Start
pedaling and you should see your wattage and cadence.

If anything goes wrong, file an issue and we'll get it fixed.

## Credits

Credit goes to Bruno Fernandez-Ruiz (Olympum) for his brilliant
little cycling power wrapper:

https://github.com/olympum/ble-cycling-power

and for his WaterRower implementation, which inspired and guided
this Trixter BLE service:

https://github.com/olympum/waterrower-ble

Also, see Bleno:

https://github.com/sandeepmistry/bleno
